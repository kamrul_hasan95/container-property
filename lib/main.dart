import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bio Data',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BioData(),
    );
  }
}

class BioData extends StatefulWidget {
  @override
  _BioDataState createState() => _BioDataState();
}

class _BioDataState extends State<BioData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.blue,
        margin: EdgeInsets.all(100),
        // padding: EdgeInsets.all(100),
        alignment: Alignment.center,
        child: Container(
          // color: Colors.red, // decoration thakle, ai color ta thakbe na, bhitor e dite hobe.
          margin: EdgeInsets.all(100),
          decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                    'https://scontent.fcla1-1.fna.fbcdn.net/v/t1.0-9/121488351_3361910197241583_4860489022137038399_o.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGAkDj0Gg6w1uKlxz-PPyqsn-oJUWLdFYif6glRYt0ViAmZIeOa1g06sVef2_ALGlYUhX2JQgzCU2zjZjMCw8oo&_nc_ohc=PvYk9e4u5uAAX98TYwb&_nc_ht=scontent.fcla1-1.fna&oh=f43e046849d6e8f4311f59403bc29d64&oe=600B2B77')),
            color: Colors.red, // gradient thakle, color thakbe na,
            shape: BoxShape.rectangle, // container ki box hobe na circle hobe
            borderRadius: BorderRadius.circular(
                360), // box container er kona gol korar jonno
            border: Border.all(
                color: Colors.white, width: 5), // shada dag dewar jonno
            // gradient: LinearGradient( //aita mela color korar jonno
            //   colors: [
            //     Colors.red,
            //     Colors.blue,
            //     Colors.green,
            //   ],
            //   stops: [
            //     0.1,
            //     0.5,
            //     0.9,
            //   ],
            // ),
            boxShadow: [
              BoxShadow(
                color: Colors.red.withAlpha(10),
                blurRadius: 10.0,
                // offset: Offset(0.0, 15.0),
              ), // shadow dewar jonno
            ],
          ),
        ),
      ),
    );
  }
}
